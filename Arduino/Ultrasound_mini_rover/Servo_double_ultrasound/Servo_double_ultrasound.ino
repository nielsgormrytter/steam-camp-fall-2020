#include <Servo.h> 
Servo Servo1; 
Servo Servo2; 

int echoPin  = 4;    
int trigPin = 5;    

void setup() { 
  Serial.begin(9600);
   Servo1.attach(8); 
   Servo2.attach(9); 
   pinMode(echoPin, INPUT);
   pinMode(trigPin, OUTPUT);
}

void loop(){ 
  digitalWrite(trigPin, LOW);   
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);  
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);   
  float pingTime = pulseIn(echoPin, HIGH);
  float distance = pingTime*0.034/2;
  Serial.println(distance);
  if (distance <= 10){
    Servo1.write(90); 
    Servo2.write(90);
    delay(2000);
  }
  else{
    Servo1.write(0); 
    Servo2.write(180); 
  }
  delay(100);
}
