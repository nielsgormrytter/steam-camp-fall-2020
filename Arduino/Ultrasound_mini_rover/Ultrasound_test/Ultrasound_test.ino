int echoPin  = 4;    
int trigPin = 5;    

void setup() {
    Serial.begin(9600);
    pinMode(echoPin, INPUT);
    pinMode(trigPin, OUTPUT);
}

void loop() {
    digitalWrite(trigPin, LOW);   
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);  
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);   
    float pingTime = pulseIn(echoPin, HIGH);
    float distance = pingTime*0.034/2;
    Serial.println(distance);
    delay(1000);
}
